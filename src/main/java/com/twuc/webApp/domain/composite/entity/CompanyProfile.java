package com.twuc.webApp.domain.composite.entity;

import com.twuc.webApp.domain.composite.entity.Location;

import javax.persistence.*;

@Entity
public class CompanyProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    @Embedded
    private Location location;

    public CompanyProfile() {
    }

    public CompanyProfile(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
