package com.twuc.webApp.domain.composite.entity;

import com.twuc.webApp.domain.composite.entity.Location;

import javax.persistence.*;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "address_city", nullable = false, length = 128)),
            @AttributeOverride(name = "street", column = @Column(name = "address_street", nullable = false, length = 128)),

    })
    private Location location;

    public UserProfile() {
    }

    public UserProfile(Location location) {
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }
}
