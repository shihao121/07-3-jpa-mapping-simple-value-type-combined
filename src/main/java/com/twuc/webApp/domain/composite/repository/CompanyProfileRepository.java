package com.twuc.webApp.domain.composite.repository;

import com.twuc.webApp.domain.composite.entity.CompanyProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyProfileRepository extends JpaRepository<CompanyProfile, Long> {
}
